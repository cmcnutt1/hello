# Default is "World"
# Author: Chris McNutt (cmcnutt1@uncc.edu)

puts "What's your name"
my_name = gets.strip

puts "Hello, #{my_name}!"
